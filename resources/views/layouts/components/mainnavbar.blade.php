<div class="container">

    <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <h1 class="text-light"><a href="#intro" class="scrollto"><span>Tangaza</span></a></h1>
{{--        <a href="#header" class="scrollto"><img src="{{asset('tangaza-logo.png')}}" alt="" class="img-fluid"></a>--}}
    </div>

    <nav class="main-nav float-right d-none d-lg-block">
        <ul>
            <li class="{{Request::is("/#intro")?"active":""}}">
                <a href="{{Route::currentRouteName()==="/"?"#intro":"/#intro"}}">Home</a>
            </li>
            <li class="{{Request::is("/#about")?"active":""}}">
                <a href="{{Route::currentRouteName()==="/"?"#about":"/#about"}}">About Us</a>
            </li>
            <li class="{{Request::is("/#services")?"active":""}}">
                <a href="{{Route::currentRouteName()==="/"?"#services":"/#services"}}">Services</a>
            </li>
            <li class="{{Request::is("/#portfolio")?"active":""}}">
                <a href="{{Route::currentRouteName()==="/"?"#portfolio":"/#portfolio"}}">Portfolio</a>
            </li>
            <li class="{{Request::is("/#team")?"active":""}}">
                <a href="{{Route::currentRouteName()==="/"?"#team":"/#team"}}">Team</a>
            </li>
{{--            <li class="drop-down"><a href="">Drop Down</a>--}}
{{--                <ul>--}}
{{--                    <li><a href="#">Drop Down 1</a></li>--}}
{{--                    <li class="drop-down"><a href="#">Drop Down 2</a>--}}
{{--                        <ul>--}}
{{--                            <li><a href="#">Deep Drop Down 1</a></li>--}}
{{--                            <li><a href="#">Deep Drop Down 2</a></li>--}}
{{--                            <li><a href="#">Deep Drop Down 3</a></li>--}}
{{--                            <li><a href="#">Deep Drop Down 4</a></li>--}}
{{--                            <li><a href="#">Deep Drop Down 5</a></li>--}}
{{--                        </ul>--}}
{{--                    </li>--}}
{{--                    <li><a href="#">Drop Down 3</a></li>--}}
{{--                    <li><a href="#">Drop Down 4</a></li>--}}
{{--                    <li><a href="#">Drop Down 5</a></li>--}}
{{--                </ul>--}}
{{--            </li>--}}
            <li class="{{Request::is("/#footer")?"active":""}}">
                <a href="{{Route::currentRouteName()==="/"?"#footer":"/#footer"}}">Contact Us</a>
            </li>
            @if (Route::has('login'))
                @auth
                    @if(Request::is('/'))
                        <li><a href="{{ url('/index') }}" class="btn btn-info m-2 text-white btn-sm">My Account</a></li>
                    @endif
                    <li><a href="{{ url('/custom-logout') }}" class="btn btn-dark m-2 text-white btn-sm">Logout</a></li>
                @else
                    <li><a href="{{ route('login') }}" class="btn btn-primary m-2 text-white btn-sm">Login</a></li>

{{--                    @if (Route::has('register'))--}}
{{--                        <li><a href="{{ route('register') }}" class="btn btn-success m-2 text-white">Register</a></li>--}}
{{--                    @endif--}}
                @endauth
            @endif
        </ul>
    </nav><!-- .main-nav -->

</div>
