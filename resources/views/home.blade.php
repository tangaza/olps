@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10 mt-5">
            <br/><br/><br/>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-default">
                    <li class="breadcrumb-item"><a href="{{url('/index')}}">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{($type->{'2377'})?:null}}</li>
                </ol>
            </nav>
            <div class="card">
                <div class="card-header text-center">Welcome to {{($type->{'2377'})?:null}}</div>
                <div class="card-body">
                    <div class="row align-content-center">
                        @if(!$myCompanies)
                            <div class="row m-auto">
                                <div class="alert alert-primary" role="alert">
                                    Not <strong>assigned</strong> to any {{($type->{'2376'})?:null}}!
                                </div>
                            </div>

                        @endif
                        @foreach($myCompanies as $myCompany)
                            <div class="col-md-3">
                                <div class="card" style="width: 100%;">
                                    <img src="{{asset('tangaza-logo.png')}}" class="card-img-top" alt="...">
                                    <div class="card-body">
{{--                                        <h5 class="card-title">{{(@$myCompany->company_details->{'1155'}?:null)}}</h5>--}}
                                        <p class="card-text">{{(@$myCompany->company_details->{'376'}?:null)}}.</p>
                                        <a href="{{url('/home/'.$myCompany->company_details->{'2381'}.'/'.($myCompany->{'2351'}))}}" class="btn btn-outline-info btn-sm"><strong>Go to applications</strong></a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
