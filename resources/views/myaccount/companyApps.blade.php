@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 mt-5">
                <br/><br/><br/>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-default">
                        <li class="breadcrumb-item"><a href="{{url('/index')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{url('/home/'.$type->{'2375'})}}">{{($type->{'2377'})}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{(@$pp->company_details->{'1155'}?:null)}}</li>
                    </ol>
                </nav>
                <div class="card border-info mb-3">
                    <div class="card-header text-center text-info h4 bg-transparent border-info">Subscriptions</div>
                    <div class="card-body">
                        <table class="table table-bordered table-sm table-hover table-info table-striped">
                            <thead>
                            <tr>
                                <th scope="col">App ID.</th>
                                <th scope="col">Logo</th>
                                <th scope="col">App Name</th>
                                <th scope="col">Start Date</th>
                                <th scope="col">Expiry Date</th>
                                <th scope="col">Option</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($apps as $app)
                                <tr>
                                    <th scope="row">{{@($app->{'2352'})?:null}} </th>
                                    <td> <img src="{{asset(@$app->$app_link_and_logo->{'2327'}?:null)}}" class="img-thumbnail" width="50px" alt="logo"/></td>
                                    <td>{{(@$app->app_details->{'1664'}?:null)}}</td>
                                    <td>{{@($app->{'2357'})?:null}}</td>
                                    <td>{{@($app->{'2358'})?:null}}</td>
                                    <td>
                                        <a href="{{(@$app->app_link_and_logo->{'2326'}?:null)}}" type="button" class="btn btn-outline-primary btn-sm">Got to app</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
