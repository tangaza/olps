@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 mt-5">
                <br/><br/><br/>
                <div class="card">
                    <div class="card-header text-center">Dashboard</div>
                    <div class="card-body">
                        <div class="row align-content-center">
                            @foreach($dashboard as $dashboard_item)
                                <div class="col-md-3 mb-2">
                                    <div class="card" style="width: 100%; ">
                                        <img src="{{asset('img/dashboard/'.$dashboard_item->image)}}" class="card-img-top" style="height: 100px;" alt="...">
                                        <div class="card-body">
                                            <p class="card-text text-center">{{(@$dashboard_item->{'2376'}?:null)}}.</p>
                                            <a href="{{url('/home/'.$dashboard_item->{'2375'})}}" class="btn btn-outline-info btn-sm text-center">
                                                <strong>Go to {{(@$dashboard_item->{'2377'}?:null)}}</strong>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
