<?php

namespace App\Http\Controllers;

use App\DB0006\TB208;
use App\DB0006\TB240;
use App\DB0006\TB38;
use App\DB0011\TB239;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $dashboard = TB240::getWhereNotIn([0,4,5,6,7],2380);
        foreach ($dashboard as $dashboard_item){
            $image = explode('/',$dashboard_item->{'2378'});
            $dashboard_item->image = $image[count($image)-1];
        }
        return view('index',compact('dashboard'));
    }

    public function home($id = null)
    {//2381
        $myCompanies = array();
        $type = TB240::getWhere(['2375'=>$id],true);
        $myCompanies1 = TB239::getWhere(['2353'=>Auth::user()->pws_counter],false,'2351');
        foreach ($myCompanies1 as $company){
            $cid = TB208::getWhere(['1959'=>$company->{'2351'}],true);
            $companyDetails = TB38::getWhere(['374'=>(@$cid->{'1960'}?:null),'2381'=>$id],true);
            if ($companyDetails) {
                $company->company_details = $companyDetails;
                array_push($myCompanies, $company);
            }
        }
        return view('home',compact('myCompanies','type'));
    }
}
