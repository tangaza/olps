<?php

namespace App\Http\Controllers;

use App\DB0006\TB208;
use App\DB0006\TB240;
use App\DB0006\TB38;
use App\DB0011\TB237;
use App\DB0011\TB239;
use App\DB0011\TB74;
use Illuminate\Support\Facades\Auth;

class CompanyController extends Controller
{
    public function apps($service_id,$companyPWS){
        $type = TB240::getWhere(['2375'=>$service_id],true);
        $apps = array();
        $myApplications = TB239::getWhere(['2351'=>$companyPWS,'2353'=>Auth::user()->pws_counter],false);

        //Filter only where service type is what is given on the url
        foreach ($myApplications as $myApplication){
            $cid = TB208::getWhere(['1959'=>$companyPWS],true);
            $companyDetails = TB38::getWhere(['374'=>(@$cid->{'1960'}?:null),'2381'=>$service_id],true);
            if ($companyDetails) {
                $myApplication->company_details = $companyDetails;
                array_push($apps, $myApplication);
            }
        }

        //Fetch extra info for the app
        foreach ($apps as $app){
            dd($app->company_details);
            $cid = TB208::getWhere(['1959'=>$companyPWS],true);
            $app_details = TB74::getWhere(['714'=>(@$cid->{'1960'}?:null),'1662'=>$app->{'2352'}],true);
            $app->app_details = $app_details;

            $app_link_and_logo = TB237::getWhere(['2325'=>$app->{'2352'}],true);
            $app->app_link_and_logo = $app_link_and_logo;
        }

        return view('myaccount.companyApps',compact('apps','type'));
    }
}
