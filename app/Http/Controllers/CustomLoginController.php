<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use App\DB0003\TB24;
use Illuminate\Support\Facades\Redirect;

class CustomLoginController extends Controller
{
    use authenticator;
    public function __invoke(Request $request)
    {
        //Validate request
        $request->validate([
           'phone_number'=>"required|min:7",
            'password'=>"required"
        ]);

        $phone = $request->input('phone_number');
        $password = $request->input('password');

        $user = User::where('mobile_number',$phone)->first();
        if (!$user){
            $record = TB24::getWhere(['266'=>$phone],true);
            $record->password = $password;
            if (!$record)
                return Redirect::back()->withErrors(['message'=>"No record found matching those credentials."]);

            if (!User::createUser($record))
                return Redirect::back()->withErrors(['message'=>"No record found matching those credentials."]);
        }

        //Try login
        if ($this->attemptAuth($phone,$password))
            return redirect('/index')->with(['message'=>"Login success"]);

        $record = TB24::getWhere(['266'=>$phone],true);
        $record->password = $password;
        //Attempt update then auth
        if (User::updateUser($record) && $this->attemptAuth($phone,$password))
            return redirect('/index')->with(['message'=>"Login success"]);

        //Login failed
        return Redirect::back()->withErrors(['message'=>"Invalid credentials."]);
    }
}
