<?php


namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;

trait authenticator
{
    public function attemptAuth($phone,$password){
        Auth::attempt([
            'mobile_number'=>$phone,
            'password'=>$password
        ]);

        if (Auth::user())
            return true;
        return false;
    }

    public function logout(){
        Auth::logout();
        return redirect('/');
    }
}
