<?php

namespace App\DB0006;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TB240 extends Model
{
    protected $connection = "DB0006";
    public $table = "240";
    public $incrementing = false;
    protected $guarded = [];

    /**
     *
     * Get using filter
     * @param array $filter
     * @param bool $single
     * @return TB240[]|Collection
     */
    public static function getWhere(array $filter = null,$single = false)
    {
        $criteria = 'get';
        if ($single)
            $criteria = 'first';

        if ($filter)
            return DB::connection((new self())->connection)
                ->table((new self())->table)->select('*')
                ->where($filter)->$criteria();

        return self::all();
    }

    /**
     * @param null $filter
     * @param null $column
     * @param bool $single
     * @return TB240[]|Collection
     */
    public static function getWhereNotIn($filter = null, $column = null, $single = false)
    {
        $criteria = 'get';
        if ($single)
            $criteria = 'first';

        if ($filter && $column)
            return DB::connection((new self())->connection)
                ->table((new self())->table)->select('*')
                ->whereNotIn($column,$filter)->$criteria();

        return self::all();
    }

    /**
     * @param null $filter
     * @param null $column
     * @param bool $single
     * @return TB240[]|Collection
     */
    public static function getWhereIn($filter = null, $column = null, $single = false)
    {
        $criteria = 'get';
        if ($single)
            $criteria = 'first';

        if ($filter && $column)
            return DB::connection((new self())->connection)
                ->table((new self())->table)->select('*')
                ->whereIn($column,$filter)->$criteria();

        return self::all();
    }
}
