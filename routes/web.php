<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/custom-login','CustomLoginController');
Route::get('/custom-logout','CustomLogoutController');

Auth::routes();

Route::get('/home/{service_id}', 'HomeController@home');
Route::get('/index', 'HomeController@index')->name('index');
Route::get('/home/{service_id}/{pws}','CompanyController@apps');
